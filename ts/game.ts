class Game {

    private _player: Player = new Player('player');
    private _counter: Counter = new Counter();
    private _boosters: Array<Booster> = new Array();

    private playerContainer = document.getElementById('player');

    constructor() {
        
        this._boosters[0] = new Booster("Auto poker", "../assets/hand.png", 0.1, 20);
        this._boosters[1] = new Booster("Coffee", "../assets/koffie.png", 1, 450);
        this._boosters[2] = new Booster("Appel fanboy", "../assets/fanboy.png", 5, 1100);
        this._boosters[3] = new Booster("Rimmert", "../assets/fanboy.png", 10, 5000);
        this._boosters[4] = new Booster("Code Farm", "../assets/farm.png", 100, 15000);

        this.renderBooster();
        
        this.playerContainer.addEventListener('transitionend', this.removeTransition);
    }

    /**
     * start function, only executed once
     */
    public start() {
        this._counter.renderCounterDisplay();
        this.ticker();        
    }

    public click = (e: Event) => {
        console.log((<Element>event.target).id);
        let targetId = (<Element>event.target).id; 

        if(targetId === this._player.name) {
            this.clickPlayerHandler();
        } else {
            this._boosters.map((booster, index) => {
                let boosterId = booster.id;
                
                if(targetId === boosterId){
                    this.buyBooster(index);
                } 
            });
        }
    }

    /**
    * ticker
    * This function ticks every set second.  
    */
    public ticker() {
        
        /* Check if the booster is locked or not */
        this.boosterLock();

        /* Update score with codePerSecond */
        this._counter.inCreaseCurrentScore();

        /* Render the score */
        this.renderScore();      

        setTimeout(() => {
            this.ticker();
        }, 100);
    }
    
    public clickPlayerHandler(){
        this._counter.currentScore = this._counter.increaseClick;  

        const playerContainer = document.getElementById('player');
        playerContainer.classList.add("clicked");

        this._player.playSound();
    }

    public removeTransition() {
        const playerContainer = document.getElementById('player');
        playerContainer.classList.remove("clicked");
    }

    public boosterLock() {
        let currentScore = this._counter.currentScore;
        
        this._boosters.map((booster) => {
            let currentBooster = document.getElementById(booster.id);

            if(currentScore >= booster.cost) {
                currentBooster.classList.remove('locked');
            } else {
                currentBooster.classList.add('locked');
            }
        });
    }

    public getBoosterFromArray(elementIndex: number) {
        let index = this._boosters.findIndex((booster, index) => index == elementIndex);
        return this._boosters[index];
    }

    public buyBooster(indexOfBooster: number) {        
        const tempBooster = this.getBoosterFromArray(indexOfBooster);

        if(this._counter.currentScore >= tempBooster.cost) {
            this.renderOwned(tempBooster, 1);
            this.updateScoreAfterBuy(tempBooster);
            this.updateCostAfterBuy(tempBooster);
        } else {
            console.log("You do not have enough code to trade in for a " + tempBooster.name);
        }

    }

    public updateScoreAfterBuy(booster: Booster){
        this._counter.buy = booster.cost;
        this._counter.updateCodePerSecond = booster.increase;
        this.renderCodePerSecond();
    }

    public updateCostAfterBuy(booster: Booster){
        let container = document.getElementById(booster.id).querySelector("#cost");
        let tempBooster = booster;

        tempBooster.increaseCost = Math.round(tempBooster.cost / 100 * 110);
        
        container.innerHTML = String(tempBooster.cost);
    }

    public renderScore() {
        let score = this._counter.currentScore;
        let scoreHolder = document.getElementById("scoreHolder");
        scoreHolder.innerHTML = "" + score; 
    }

    public renderBooster() {
        let boosterHolder = document.getElementById("upgrade-container"); 
        boosterHolder.innerHTML = "";
        this._boosters.map((booster) => {
            boosterHolder.appendChild(booster.boosterDomRepresentation);
        })
    }
    
    public renderOwned(booster: Booster, increase: number){
        let tempBooster = booster;
    
        tempBooster.increaseAmountOwned = increase;
    
        let boosterContainer = document.getElementById(tempBooster.id).querySelector(".right-content");
        boosterContainer.innerHTML = String(tempBooster.owned);
    }
        
    public renderCodePerSecond(){
        let container = document.getElementById("codePerSecond");

        container.innerHTML = "lines per second: " + (this._counter.currentCodePerSecond).toFixed(1);
    }
}