class Counter {

    private _currentScore: number = 0;
    private _allTimeCode: number = 0;
    private _increaseClick: number = 1; 
    private _codePerSecond: number = 0;

    constructor() {
        
    }
    
    /**
     * Set the containing amount value.
     * @param {number} - The amount value.
     */
    public set currentScore(increaseClick: number) {
        this._currentScore += increaseClick;
    }
    
    public set buy(decreaseScore: number){
        this._currentScore -= decreaseScore;
    }
    /**
    * Set the containing amount value.
    * @param {number} - The amount value.
    */
    public set updateCodePerSecond(increase: number) {
        this._codePerSecond += increase;
    }
    
    /**
     * Increase currentScore with the codePerSecond. 
     * @param {number} - The amount value.
     */
    public inCreaseCurrentScore(){
        this._currentScore += this._codePerSecond / 1000 * 100;
    }

    /**
     * Get the amount value.
     * @return {number} The amount value.
     */
    public get currentScore(): number{
        return Math.floor(this._currentScore);
    }

    public get currentCodePerSecond(){
        return this._codePerSecond;
    }

    /**
     * Get the amount value.
     * @return {number} The amount value.
     */
    public get increaseClick(): number{
        return this._increaseClick;
    }

    public renderCounterDisplay(){
        let container = document.getElementById('container');
        let counterDiv = document.createElement('div');
        counterDiv.id = 'scoreCounter';
        
        let counter = container.appendChild(counterDiv);
        let player = document.getElementById('inner');
        container.insertBefore(counter, player);

        let scoreHolder = document.createElement('span');
        scoreHolder.id = 'scoreHolder';
        counterDiv.appendChild(scoreHolder);

        scoreHolder.innerHTML += this._currentScore;
        counterDiv.innerHTML += " lines of code";

        let codePerSecondContainer = document.createElement('div');
        codePerSecondContainer.id = 'codePerSecond';

        codePerSecondContainer.innerHTML = "lines per second: " + this._codePerSecond;

        counterDiv.appendChild(codePerSecondContainer);
        

    }

}