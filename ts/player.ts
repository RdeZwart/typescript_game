class Player {

    private _id: string;

    constructor(name: string) {
        this._id = name; 

        this.renderPlayer();
    }

    public get name(){
        return this._id;
    }

    public playSound(){
        const sound1: HTMLAudioElement = new Audio("assets/audio/click.wav");
        const sound2: HTMLAudioElement = new Audio("assets/audio/click_2.wav");
        const sound3: HTMLAudioElement = new Audio("assets/audio/click_3.wav");
        const sound4: HTMLAudioElement = new Audio("assets/audio/click_4.wav");

        let audioArray = [sound1, sound2, sound3, sound4]; 

        var index = Math.floor(Math.random() * (audioArray.length)),
        thisSound = audioArray[index];

        thisSound.play();
    }    

    /**
     * Get the dom representation of the player.
     * @return {HTMLDivElement} - the player dom value.
     */
    public renderPlayer() {
        let container = document.getElementById('inner');
        let playerDiv = document.createElement('div');
            
        playerDiv.id = this._id;
            
        container.appendChild(playerDiv);
    }

}