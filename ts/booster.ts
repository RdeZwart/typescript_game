class Booster {

    private _boosterId: string;
    private _boosterName: string;
    private _boosterImage: string; 
    private _amountOwned: number = 0;
    private _increase: number; 
    private _increaseClickPerSecond: number;
    private _cost: number;

    constructor(name: string, image: string, increase: number, cost: number) {
        this._boosterName = name;
        this._boosterImage = image;
        this._increase = increase; 
        this._cost = cost;

        this._boosterId = name.toLowerCase().replace(/\s+/g, '');
    }

    public set increaseCost(increaseCost: number){
        this._cost = increaseCost;
    }

    public set increaseAmountOwned(increase: number){
        this._amountOwned += increase;
    } 
    
    public get id(){
        return this._boosterId;
    }
    
    public get name(){
        return this._boosterName;
    }

    public get cost(){
        return this._cost;
    }
    public get increase(){
        return this._increase;
    }

    public get owned(){
        return this._amountOwned;
    }

    /**
     * Get the amount value.
     * @return {HTMLDivElement} the cup dom representation
     */
    public get boosterDomRepresentation(): HTMLDivElement {
        let div = document.createElement('div');
        div.id = this._boosterId;

        div.classList.add("booster", "locked");

        let amount = " " + this._amountOwned;

        let boosterContent = `<div class='left-content'><h1>${this._boosterName}</h1> <i class="fas fa-code"></i> <span id="cost">${this._cost}</span></div><div class='right-content'>${amount}</div>`; 
        div.innerHTML = boosterContent;

        return div;
    }

}