class Achievement {
    constructor() {
    }
}
class Booster {
    constructor(name, image, increase, cost) {
        this._amountOwned = 0;
        this._boosterName = name;
        this._boosterImage = image;
        this._increase = increase;
        this._cost = cost;
        this._boosterId = name.toLowerCase().replace(/\s+/g, '');
    }
    set increaseCost(increaseCost) {
        this._cost = increaseCost;
    }
    set increaseAmountOwned(increase) {
        this._amountOwned += increase;
    }
    get id() {
        return this._boosterId;
    }
    get name() {
        return this._boosterName;
    }
    get cost() {
        return this._cost;
    }
    get increase() {
        return this._increase;
    }
    get owned() {
        return this._amountOwned;
    }
    get boosterDomRepresentation() {
        let div = document.createElement('div');
        div.id = this._boosterId;
        div.classList.add("booster", "locked");
        let amount = " " + this._amountOwned;
        let boosterContent = `<div class='left-content'><h1>${this._boosterName}</h1> <i class="fas fa-code"></i> <span id="cost">${this._cost}</span></div><div class='right-content'>${amount}</div>`;
        div.innerHTML = boosterContent;
        return div;
    }
}
class Counter {
    constructor() {
        this._currentScore = 0;
        this._allTimeCode = 0;
        this._increaseClick = 1;
        this._codePerSecond = 0;
    }
    set currentScore(increaseClick) {
        this._currentScore += increaseClick;
    }
    set buy(decreaseScore) {
        this._currentScore -= decreaseScore;
    }
    set updateCodePerSecond(increase) {
        this._codePerSecond += increase;
    }
    inCreaseCurrentScore() {
        this._currentScore += this._codePerSecond / 1000 * 100;
    }
    get currentScore() {
        return Math.floor(this._currentScore);
    }
    get currentCodePerSecond() {
        return this._codePerSecond;
    }
    get increaseClick() {
        return this._increaseClick;
    }
    renderCounterDisplay() {
        let container = document.getElementById('container');
        let counterDiv = document.createElement('div');
        counterDiv.id = 'scoreCounter';
        let counter = container.appendChild(counterDiv);
        let player = document.getElementById('inner');
        container.insertBefore(counter, player);
        let scoreHolder = document.createElement('span');
        scoreHolder.id = 'scoreHolder';
        counterDiv.appendChild(scoreHolder);
        scoreHolder.innerHTML += this._currentScore;
        counterDiv.innerHTML += " lines of code";
        let codePerSecondContainer = document.createElement('div');
        codePerSecondContainer.id = 'codePerSecond';
        codePerSecondContainer.innerHTML = "lines per second: " + this._codePerSecond;
        counterDiv.appendChild(codePerSecondContainer);
    }
}
class Game {
    constructor() {
        this._player = new Player('player');
        this._counter = new Counter();
        this._boosters = new Array();
        this.playerContainer = document.getElementById('player');
        this.click = (e) => {
            console.log(event.target.id);
            let targetId = event.target.id;
            if (targetId === this._player.name) {
                this.clickPlayerHandler();
            }
            else {
                this._boosters.map((booster, index) => {
                    let boosterId = booster.id;
                    if (targetId === boosterId) {
                        this.buyBooster(index);
                    }
                });
            }
        };
        this._boosters[0] = new Booster("Auto poker", "../assets/hand.png", 0.1, 20);
        this._boosters[1] = new Booster("Coffee", "../assets/koffie.png", 1, 450);
        this._boosters[2] = new Booster("Appel fanboy", "../assets/fanboy.png", 5, 1100);
        this._boosters[3] = new Booster("Rimmert", "../assets/fanboy.png", 10, 5000);
        this._boosters[4] = new Booster("Code Farm", "../assets/farm.png", 100, 15000);
        this.renderBooster();
        this.playerContainer.addEventListener('transitionend', this.removeTransition);
    }
    start() {
        this._counter.renderCounterDisplay();
        this.ticker();
    }
    ticker() {
        this.boosterLock();
        this._counter.inCreaseCurrentScore();
        this.renderScore();
        setTimeout(() => {
            this.ticker();
        }, 100);
    }
    clickPlayerHandler() {
        this._counter.currentScore = this._counter.increaseClick;
        const playerContainer = document.getElementById('player');
        playerContainer.classList.add("clicked");
        this._player.playSound();
    }
    removeTransition() {
        const playerContainer = document.getElementById('player');
        playerContainer.classList.remove("clicked");
    }
    boosterLock() {
        let currentScore = this._counter.currentScore;
        this._boosters.map((booster) => {
            let currentBooster = document.getElementById(booster.id);
            if (currentScore >= booster.cost) {
                currentBooster.classList.remove('locked');
            }
            else {
                currentBooster.classList.add('locked');
            }
        });
    }
    getBoosterFromArray(elementIndex) {
        let index = this._boosters.findIndex((booster, index) => index == elementIndex);
        return this._boosters[index];
    }
    buyBooster(indexOfBooster) {
        const tempBooster = this.getBoosterFromArray(indexOfBooster);
        if (this._counter.currentScore >= tempBooster.cost) {
            this.renderOwned(tempBooster, 1);
            this.updateScoreAfterBuy(tempBooster);
            this.updateCostAfterBuy(tempBooster);
        }
        else {
            console.log("You do not have enough code to trade in for a " + tempBooster.name);
        }
    }
    updateScoreAfterBuy(booster) {
        this._counter.buy = booster.cost;
        this._counter.updateCodePerSecond = booster.increase;
        this.renderCodePerSecond();
    }
    updateCostAfterBuy(booster) {
        let container = document.getElementById(booster.id).querySelector("#cost");
        let tempBooster = booster;
        tempBooster.increaseCost = Math.round(tempBooster.cost / 100 * 110);
        container.innerHTML = String(tempBooster.cost);
    }
    renderScore() {
        let score = this._counter.currentScore;
        let scoreHolder = document.getElementById("scoreHolder");
        scoreHolder.innerHTML = "" + score;
    }
    renderBooster() {
        let boosterHolder = document.getElementById("upgrade-container");
        boosterHolder.innerHTML = "";
        this._boosters.map((booster) => {
            boosterHolder.appendChild(booster.boosterDomRepresentation);
        });
    }
    renderOwned(booster, increase) {
        let tempBooster = booster;
        tempBooster.increaseAmountOwned = increase;
        let boosterContainer = document.getElementById(tempBooster.id).querySelector(".right-content");
        boosterContainer.innerHTML = String(tempBooster.owned);
    }
    renderCodePerSecond() {
        let container = document.getElementById("codePerSecond");
        container.innerHTML = "lines per second: " + (this._counter.currentCodePerSecond).toFixed(1);
    }
}
const app = {};
(function () {
    let init = function () {
        app.game = new Game();
        app.game.start();
    };
    let clickEvent = function () {
        app.game.click();
    };
    window.addEventListener('load', init);
    window.addEventListener('click', clickEvent);
})();
class Player {
    constructor(name) {
        this._id = name;
        this.renderPlayer();
    }
    get name() {
        return this._id;
    }
    playSound() {
        const sound1 = new Audio("assets/audio/click.wav");
        const sound2 = new Audio("assets/audio/click_2.wav");
        const sound3 = new Audio("assets/audio/click_3.wav");
        const sound4 = new Audio("assets/audio/click_4.wav");
        let audioArray = [sound1, sound2, sound3, sound4];
        var index = Math.floor(Math.random() * (audioArray.length)), thisSound = audioArray[index];
        thisSound.play();
    }
    renderPlayer() {
        let container = document.getElementById('inner');
        let playerDiv = document.createElement('div');
        playerDiv.id = this._id;
        container.appendChild(playerDiv);
    }
}
//# sourceMappingURL=main.js.map